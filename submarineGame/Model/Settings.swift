import Foundation

class Settings: Codable{
    
    var fish: Int = 0
    var ship: Int = 0
    var submarine: Int = 0
    var speed : Int = 1
    var name: String = "User"
    var switchControl : Bool = true
    
    
    init(fish: Int, ship: Int, submarine: Int, name: String, speed: Int, switchControl: Bool){
        self.fish = fish
        self.ship = ship
        self.submarine = submarine
        self.name = name
        self.speed = speed
        self.switchControl = switchControl
    }
    
    enum CodingKeys: String, CodingKey {
        case fish, ship, submarine, name, speed, switchControl
    }

    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.fish = try container.decode(Int.self, forKey: .fish)
        self.ship = try container.decode(Int.self, forKey: .ship)
        self.submarine = try container.decode(Int.self, forKey: .submarine)
        self.name = try container.decode(String.self, forKey: .name)
        self.speed = try container.decode(Int.self, forKey: .speed)
        self.switchControl = try container.decode(Bool.self, forKey: .switchControl)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(self.fish, forKey: .fish)
        try container.encode(self.ship, forKey: .ship)
        try container.encode(self.submarine, forKey: .submarine)
        try container.encode(self.name, forKey: .name)
        try container.encode(self.speed, forKey: .speed)
        try container.encode(self.switchControl, forKey: .switchControl)

      }
}

extension UserDefaults {
    
    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }
    
    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
            let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}
