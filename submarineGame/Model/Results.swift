import Foundation

class Results: Codable{
    
    var resultArray  : [String] = []
    var dateResultArray : [String] = []
    var userNameArray : [String] = []
    
    init(resultsArray: [String], dateResultArray : [String], userNameArray : [String]){
        self.resultArray = resultsArray
        self.dateResultArray = dateResultArray
        self.userNameArray = userNameArray
    }
    
    enum CodingKeys: String, CodingKey {
        case resultArray, dateResultArray, userNameArray
    }

    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.resultArray = try container.decode([String].self, forKey: .resultArray)
        self.dateResultArray = try container.decode([String].self, forKey: .dateResultArray)
        self.userNameArray = try container.decode([String].self, forKey: .userNameArray)

    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(self.resultArray, forKey: .resultArray)
        try container.encode(self.dateResultArray, forKey: .dateResultArray)
        try container.encode(self.userNameArray, forKey: .userNameArray)

        
      }
}
