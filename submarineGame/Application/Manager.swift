import Foundation

class Manager {
    
    static let shared = Manager()
    private init(){}
    
    var indexOfShip : Int = 0
    var indexOfSubmarine : Int = 0
    var indexOfFish : Int = 0
    var indexOfSpeed : Int = 1
    var switchControl : Bool = true
    
    var dateResultArray : [String] = []
    var userNameArray : [String] = []
    var resultArray  : [String] = []
    
    var submarineArray = ["submarineInGame", "submarine2", "submarine3"]
    var shipArray = ["ship", "ship2", "ship3"]
    var fishArray = ["fish","fish2","fish3"]
}
