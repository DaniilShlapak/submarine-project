import UIKit

class AchievementTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var resultLabel: UILabel!
        
    func configure(result: String, name : String, date : String){
        userNameLabel.text = name
        dateLabel.text = date
        resultLabel.text = result
    }
}
