import UIKit
import CoreMotion

class GameViewController: UIViewController {
    //MARK: Outlets
    @IBOutlet weak var startAgainButton: UIImageView!
    @IBOutlet weak var resumeButton: UIImageView!
    @IBOutlet weak var viewUpMoveView: UIView!
    @IBOutlet weak var viewBottomMoveView: UIView!
    @IBOutlet weak var dnoView: UIImageView!
    @IBOutlet weak var skyView: UIImageView!
    @IBOutlet weak var gameOverView: UIView!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var mainBlur: UIVisualEffectView!
    @IBOutlet weak var scoreLabe: UILabel!
    @IBOutlet weak var viewForObjects: UIView!
    @IBOutlet weak var pauseButton: UIImageView!
    @IBOutlet weak var exitButton: UIImageView!
    
    //MARK: LETS
    let motionManager = CMMotionManager()
    
    //MARK: Vars
    var bottomObstacle = UIImageView()
    var rockFlag = true
    var speed = 1
    var userName = "User"
    var airCheckMove = true
    var results : Results?
    var airCheckView = UIProgressView()
    var submarineView = UIImageView()
    var fishImageView = UIImageView()
    var shipImageView = UIImageView()
    var settings = UserDefaults.standard.value(Settings.self, forKey: "GameSettings")
    var flagFish = true
    var flagShip = true
    var flagMove = true
    var timer = Timer()
    var timerOfProgress = Timer()
    var resultsTimer = Timer()
    var timerOfFish = Timer()
    var timerOfShip = Timer()
    var timerOfRock = Timer()
    var textInTimerLabel = 0
    var scoreMove = true
    var arrayForSave = [String]()
    var intersectionFlag = true
    
    //MARK: Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        startGame()
    }
    
    //MARK: Flow functions
    private func createRecognizers(){
        if Manager.shared.switchControl {
            let upRecognizer = UITapGestureRecognizer(target: self, action: #selector(upArrowTapped))
            self.viewUpMoveView.addGestureRecognizer(upRecognizer)
            
            let bottomRecognizer = UITapGestureRecognizer(target: self, action: #selector(bottomArrowTapped))
            self.viewBottomMoveView.addGestureRecognizer(bottomRecognizer)
        }
        
        let pauseRecognizer = UITapGestureRecognizer(target: self, action: #selector(pauseButtonPressed))
        self.pauseButton.addGestureRecognizer(pauseRecognizer)
        
        let exitRecognizer = UITapGestureRecognizer(target: self, action: #selector(exitButtonPressed))
        self.exitButton.addGestureRecognizer(exitRecognizer)
        
        let resumeRecognizer = UITapGestureRecognizer(target: self, action: #selector(resumeButtonPressed))
        self.resumeButton.addGestureRecognizer(resumeRecognizer)
        
        let restartRecognizer = UITapGestureRecognizer(target: self, action: #selector(restartGame))
        self.startAgainButton.addGestureRecognizer(restartRecognizer)
    }
    
    @IBAction func restartGame(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "GameViewController")
        var viewcontrollers = self.navigationController?.viewControllers
        viewcontrollers?.removeLast()
        viewcontrollers?.append(vc)
        self.navigationController?.setViewControllers(viewcontrollers!, animated: true)
    }
    
    private func startGame(){
        intersectionFlag = true
        flagFish = true
        flagShip = true
        flagMove = true
        rockFlag = true
        mainBlur.isHidden = true
        gameOverView.isHidden = true
        airCheckView.progress = 1
        airCheckMove = true
        timer.fire()
        timerOfFish.fire()
        timerOfShip.fire()
        resultsTimer.fire()
        timerOfRock.fire()
        airCheck()
        showSubmarine()
        createRecognizers()
        
        if Manager.shared.switchControl == false {
            motionCheck()
        }
        
        timerOfFish = Timer.scheduledTimer(withTimeInterval: timeOfFishAppear() , repeats: true) { _ in
            if self.flagFish{
                self.showFish()
            }
        }
        
        timerOfShip = Timer.scheduledTimer(withTimeInterval: timeOfShipAppear() , repeats: true) { _ in
            if self.flagShip{
                self.showShip()
            }
        }
        
        timerOfRock = Timer.scheduledTimer(withTimeInterval: timeOfAppear() , repeats: true) { _ in
            if self.rockFlag {
                self.createBottomObstacle()
            }
        }

        
        timer = Timer.scheduledTimer(withTimeInterval: 0.2 , repeats: true) { _ in
            self.moveShip()
            self.moveFish()
            self.moveRock()
        }
        
        if let savedSpeed = settings?.speed{
            speed = savedSpeed
        }
        
        resultsTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { _ in
            if self.scoreMove{
                self.textInTimerLabel += 1 * self.speed
                self.timerLabel.text = String(self.textInTimerLabel) + " \(NSLocalizedString("m", comment: ""))"
            }
        })
    }
    
    @IBAction func pauseButtonPressed(){
        pause()
    }
    
    @IBAction func resumeButtonPressed() {
        flagMove = true
        intersectionFlag = true
        mainBlur.isHidden = true
        gameOverView.isHidden = true
        airCheckMove = true
        scoreMove = true
    }
    
    @IBAction func pause(){
        self.airCheckMove = false
        self.flagFish = false
        self.flagShip = false
        self.flagMove = false
        self.gameOverView.isHidden = false
        self.mainBlur.isHidden = false
        self.scoreLabe.text = userName + ", \(NSLocalizedString("your score", comment: "")) " + String(self.textInTimerLabel) + " \(NSLocalizedString("m", comment: ""))"
        self.scoreLabe.textAlignment = .center
        self.scoreMove = false
        self.intersectionFlag = false
        saveResult()
    }
    
    private func showSubmarine(){
        if let submarineView = settings?.submarine{
            let submarineImage = UIImage(named: Manager.shared.submarineArray[submarineView])
            self.submarineView.image = submarineImage
        }else {
            let submarineImage = UIImage(named: Manager.shared.submarineArray[0])
            self.submarineView.image = submarineImage
        }
        self.submarineView.frame = CGRect(x: 0, y: view.center.y - self.submarineView.frame.height / 2, width: self.view.frame.width / 8, height: self.view.frame.height / 7)
        self.submarineView.contentMode = .scaleAspectFit
        self.viewForObjects.addSubview(self.submarineView)
        createAirCheck()
    }
    
    private func createAirCheck(){
        self.airCheckView.frame = CGRect(x: 0, y: (self.view.frame.height / 2 - self.submarineView.frame.height / 2) + self.submarineView.frame.height / 3, width: self.submarineView.frame.width * 0.7, height: self.submarineView.frame.height / 6)
        self.airCheckView.center.x = self.submarineView.center.x
        self.viewForObjects.addSubview(airCheckView)
        self.airCheckView.setProgress(1, animated: true)
    }
    
    private func gameOver(){
        if let name = settings?.name {
            self.userName = name
        }

        self.airCheckMove = false
        self.flagFish = false
        self.flagShip = false
        self.flagMove = false
        self.gameOverView.isHidden = false
        self.mainBlur.isHidden = false
        self.scoreLabe.text = userName + ", \(NSLocalizedString("your score", comment: "")) " + String(self.textInTimerLabel) + " \(NSLocalizedString("m", comment: ""))"
        self.scoreLabe.textAlignment = .center
        self.scoreMove = false
        self.intersectionFlag = false
        saveResult()
    }
    
    private func getDate() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM HH:mm"
        let dateNow = formatter.string(from: Date())
        
        return dateNow
    }
    
    private func saveResult(){
        if let name = settings?.name {
            self.userName = name
        }
        
        Manager.shared.resultArray.append(String(self.textInTimerLabel))
        Manager.shared.dateResultArray.append(getDate())
        Manager.shared.userNameArray.append(userName)
        
        let saveResults = Results(resultsArray: Manager.shared.resultArray, dateResultArray: Manager.shared.dateResultArray, userNameArray: Manager.shared.userNameArray)
        
        UserDefaults.standard.set(encodable: saveResults, forKey: "resultArray")
    }
    
    private func airCheck(){
        if let savedSpeed = settings?.speed{
            speed = savedSpeed
        }
            timerOfProgress = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { _ in
                UIView.animate(withDuration: 0.3) {
                    if self.airCheckMove {
                        if self.submarineView.frame.origin.y > (self.submarineView.frame.height / 1.5){
                            self.airCheckView.progress -= 0.004 * Float(self.speed)
                        }else {
                            self.airCheckView.progress += 0.03
                        }
                        if self.airCheckView.progress == 0{
                            print("low aircheck")
                            self.gameOver()
                        }
                    }

                }
            })
    }
    
    private func motionCheck(){
        if motionManager.isAccelerometerAvailable {
            motionManager.accelerometerUpdateInterval = 0.1
            motionManager.startAccelerometerUpdates(to: .main) { [weak self] (data: CMAccelerometerData?, error: Error?) in
                if let acceleration = data?.acceleration {
                    print("x = " + "\(acceleration.x)")
                    print("y = " + "\(acceleration.y)")
                    print("z = " +  "\(acceleration.z)")
                    print()
                    if acceleration.y > 0.1{
                        self?.upArrowTapped()
                    }
                    else if acceleration.y <= -0.09{
                        self?.bottomArrowTapped()
                    }
                }
            }
        }
    }
    
    @IBAction func upArrowTapped(){
        if self.submarineView.frame.origin.y > self.submarineView.frame.height / 1.5 {
            UIView.animate(withDuration: 0.3) {
                self.submarineView.frame.origin.y -= 20
                self.airCheckView.frame.origin.y -= 20
            }
        }
    }
    
    @IBAction func bottomArrowTapped(){
        if self.submarineView.frame.origin.y < (self.view.frame.height - self.submarineView.frame.height - self.airCheckView.frame.height) {
            UIView.animate(withDuration: 0.3) {
                self.submarineView.frame.origin.y += 20
                self.airCheckView.frame.origin.y += 20
            }
        }
    }
    
    private func moveFish(){
        if let savedSpeed = settings?.speed{
            speed = savedSpeed
        }

        if flagMove{
            UIView.animate(withDuration: 0.3) {
                self.fishImageView.frame.origin.x -= 20 * CGFloat(self.speed)
            }
            if self.fishImageView.frame.origin.x + self.fishImageView.frame.width < 0{
                flagFish = !flagFish
                self.fishImageView.removeFromSuperview()
            }
        }
        intersectsDetected()
    }
    
    private func moveShip(){
        if let savedSpeed = settings?.speed{
            speed = savedSpeed
        }

        if flagMove{
            UIView.animate(withDuration: 0.3) {
                self.shipImageView.frame.origin.x -= 15 * CGFloat(self.speed)
            }
            if self.shipImageView.frame.origin.x + self.shipImageView.frame.width < 0{
                flagShip = !flagShip
                self.shipImageView.removeFromSuperview()
            }
        }
        intersectsDetected()
    }
    
    private func moveRock(){
        if let savedSpeed = settings?.speed{
            speed = savedSpeed
        }
        
        if flagMove {
            UIView.animate(withDuration: 0.3) {
                self.bottomObstacle.frame.origin.x -= 15 * CGFloat(self.speed)
            }
            if self.bottomObstacle.frame.origin.x + self.bottomObstacle.frame.width < 0{
                self.rockFlag = !self.rockFlag
                self.bottomObstacle.removeFromSuperview()
            }
        }
        intersectsDetected()
    }
    
    private func showFish(){
        
        if let settingFish = settings?.fish{
            let fish = UIImage(named:Manager.shared.fishArray[settingFish])
            self.fishImageView.image = fish
        }else {
            let fish = UIImage(named:Manager.shared.fishArray[0])
            self.fishImageView.image = fish
        }
        self.fishImageView.frame = CGRect(x: self.view.frame.width, y: appearFishPositionY(), width: self.view.frame.width / 8, height: self.view.frame.height / 6)
        self.fishImageView.contentMode = .scaleAspectFit
        self.viewForObjects.addSubview(self.fishImageView)
        flagFish = false
    }
    
    private func showShip(){
        if let shipImage = settings?.ship{
            let shipImage = UIImage(named: Manager.shared.shipArray[shipImage])
            self.shipImageView.image = shipImage
        }else {
            let shipImage = UIImage(named: Manager.shared.shipArray[0])
            self.shipImageView.image = shipImage
        }
        self.shipImageView.frame = CGRect(x: self.view.frame.width, y: 0, width: self.submarineView.frame.width * 1.5, height: skyView.frame.height * 1.3)
        self.shipImageView.contentMode = .scaleAspectFit
        self.viewForObjects.addSubview(self.shipImageView)
        flagShip = false
    }
    
    private func createBottomObstacle(){
        bottomObstacle.image = UIImage(named: "rock2")
        bottomObstacle.frame = CGRect(x: self.view.frame.width, y: self.view.frame.height - self.view.frame.height / 15, width: self.submarineView.frame.width * 1.2, height: self.view.frame.height / 15)
        bottomObstacle.contentMode = .scaleAspectFill
        self.viewForObjects.addSubview(self.bottomObstacle)
        self.rockFlag = false
    }
    
    private func appearFishPositionY() -> CGFloat{
        let randomY = CGFloat.random(in: self.skyView.frame.height...(self.viewForObjects.frame.height - (self.fishImageView.frame.height + self.bottomObstacle.frame.height - self.skyView.frame.height)))
        return randomY
    }
    
    private func timeOfFishAppear() -> Double{
        let time = Double.random(in: 0.1...1)
        return time
    }
    
    private func timeOfShipAppear() -> Double{
        let time = Double.random(in: 0.1...1)
        return time
    }
    
    private func timeOfAppear() -> Double {
        let time = Double.random(in: 0.1...1)
        return time
    }
    
    private func intersectsDetected(){
        guard let presentationFishFrame = fishImageView.layer.presentation()?.frame else {return}
        guard let presentationShipFrame = shipImageView.layer.presentation()?.frame else {return}
        guard let presentationSubmarineFrame = self.submarineView.layer.presentation()?.frame else {return}
        guard let presentationObstacleFrame = self.bottomObstacle.layer.presentation()?.frame else {return}
        
        if intersectionFlag{
            if presentationFishFrame.intersects(presentationSubmarineFrame) || presentationShipFrame.intersects(presentationSubmarineFrame) || presentationObstacleFrame.intersects(presentationSubmarineFrame){
                gameOver()
            }
        }
    }
    
    @IBAction func exitButtonPressed(){
        self.navigationController?.popToRootViewController(animated: true)
    }
}
