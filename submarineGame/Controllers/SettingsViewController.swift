import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var submarineSettingsImageView: UIImageView!
    @IBOutlet weak var shipImageView: UIImageView!
    @IBOutlet weak var fishImageView: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var speedLabel: UILabel!
    @IBOutlet weak var controlSwitch: UISwitch!
    
    var settings : Settings?
   
    override func viewDidLoad() {
        super.viewDidLoad()
        loadSettings()
        setSettings()
    }
    
    private func loadSettings(){
        submarineSettingsImageView.image = UIImage(named: Manager.shared.submarineArray[Manager.shared.indexOfSubmarine])
        shipImageView.image = UIImage(named: Manager.shared.shipArray[Manager.shared.indexOfShip])
        fishImageView.image = UIImage(named: Manager.shared.fishArray[Manager.shared.indexOfFish])
        speedLabel.text = String(Manager.shared.indexOfSpeed)
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        guard let textFromField = nameTextField.text else {return}
        
        let fishSetting = Settings(fish: Manager.shared.indexOfFish, ship: Manager.shared.indexOfShip, submarine: Manager.shared.indexOfSubmarine, name: textFromField, speed: Manager.shared.indexOfSpeed, switchControl: Manager.shared.switchControl)
        UserDefaults.standard.set(encodable: fishSetting, forKey: "GameSettings")
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func switchPressed(_ sender: UISwitch) {
        Manager.shared.switchControl = controlSwitch.isOn
    }
    @IBAction func previousButtonPressed(_ sender: UIButton) {
        if Manager.shared.indexOfSubmarine > 0{
            Manager.shared.indexOfSubmarine -= 1
            let submarineImage = UIImage(named: Manager.shared.submarineArray[Manager.shared.indexOfSubmarine])
            self.submarineSettingsImageView.image = submarineImage
        }
    }
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        if Manager.shared.submarineArray.count - 1 > Manager.shared.indexOfSubmarine{
            Manager.shared.indexOfSubmarine += 1
            let submarineImage = UIImage(named: Manager.shared.submarineArray[Manager.shared.indexOfSubmarine])
            self.submarineSettingsImageView.image = submarineImage
        }
    }
    
    @IBAction func nextShipButtonPressed(_ sender: UIButton) {
        if Manager.shared.shipArray.count - 1 > Manager.shared.indexOfShip{
            Manager.shared.indexOfShip += 1
            UIView.animate(withDuration: 0.3) {
                let shipImage = UIImage(named: Manager.shared.shipArray[Manager.shared.indexOfShip ])
                self.shipImageView.image = shipImage
            }
        }
    }

    @IBAction func previousShipButtonPressed(_ sender: UIButton) {
        if Manager.shared.indexOfShip > 0{
            Manager.shared.indexOfShip  -= 1
            UIView.animate(withDuration: 0.3) {
                let shipImage = UIImage(named: Manager.shared.shipArray[Manager.shared.indexOfShip ])
                self.shipImageView.image = shipImage
            }
        }
    }

    @IBAction func nextFishButtonPressed(_ sender: UIButton) {
        if Manager.shared.fishArray.count - 1 > Manager.shared.indexOfFish{
            Manager.shared.indexOfFish += 1
            UIView.animate(withDuration: 0.3) {
                let fishImage = UIImage(named: Manager.shared.fishArray[Manager.shared.indexOfFish])
                self.fishImageView.image = fishImage
            }
        }
    }

    @IBAction func previousFishButtonPressed(_ sender: UIButton) {
        if Manager.shared.indexOfFish > 0{
            Manager.shared.indexOfFish -= 1
            UIView.animate(withDuration: 0.3) {
                let fishImage = UIImage(named: Manager.shared.fishArray[Manager.shared.indexOfFish])
                self.fishImageView.image = fishImage
            }
        }
    }
    
    @IBAction func reduceSpeedButtonPressed(_ sender: UIButton){
        if Manager.shared.indexOfSpeed > 1 {
            Manager.shared.indexOfSpeed -= 1
        }
        speedLabel.text = String(Manager.shared.indexOfSpeed)
    }

    @IBAction func enlargeSpeedButtonPressed(_ sender: UIButton) {
        if Manager.shared.indexOfSpeed < 5{
            Manager.shared.indexOfSpeed += 1
        }
        speedLabel.text = String(Manager.shared.indexOfSpeed)
    }
    
    private func settingsShowed(){
        guard  let showedSettings = UserDefaults.standard.value(Settings.self, forKey: "GameSettings") else {
            return
        }
        settings = showedSettings
    }
    
    private func setSettings(){
        settingsShowed()
        
        guard let setSettings = settings else {return}
        submarineSettingsImageView.image = UIImage(named: Manager.shared.submarineArray[setSettings.submarine])
        shipImageView.image = UIImage(named: Manager.shared.shipArray[setSettings.ship])
        fishImageView.image = UIImage(named: Manager.shared.fishArray[setSettings.fish])
        nameTextField.text = setSettings.name
        Manager.shared.indexOfSubmarine = setSettings.submarine
        Manager.shared.indexOfShip = setSettings.ship
        Manager.shared.indexOfFish = setSettings.fish
        Manager.shared.indexOfSpeed = setSettings.speed
        speedLabel.text = String(setSettings.speed)
        Manager.shared.switchControl = setSettings.switchControl
        controlSwitch.isOn = setSettings.switchControl
    }
}
