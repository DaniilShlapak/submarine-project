import UIKit

class AchievementsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension AchievementsViewController: UITableViewDelegate, UITableViewDataSource{
    func changeColor(cell: AchievementTableViewCell) {
        view.backgroundColor = cell.contentView.backgroundColor
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Manager.shared.resultArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AchievementTableViewCell", for: indexPath) as? AchievementTableViewCell else {return UITableViewCell()}
        
        cell.configure(result: Manager.shared.resultArray[indexPath.row], name: Manager.shared.userNameArray[indexPath.row], date: Manager.shared.dateResultArray[indexPath.row])
        return cell
    }
}
