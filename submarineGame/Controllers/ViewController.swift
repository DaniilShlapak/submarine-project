import UIKit
import SpriteKit

class ViewController: UIViewController {

    private let animationView = SKView()
    
    @IBOutlet weak var backgroungImageView: UIImageView!
    @IBOutlet weak var menuImageView: UIImageView!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var achievementsButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    
    let results = UserDefaults.standard.value(Results.self, forKey: "resultArray")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    private func setup(){
        self.backgroungImageView.addParalaxEffect()
        self.startButton.dropShadow()
        self.achievementsButton.dropShadow()
        self.settingsButton.dropShadow()
        self.getSaves()
        setupSKScene()
    }
    
    private func setupSKScene(){
        animationView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width / 2, height: self.view.frame.height)
        self.view.addSubview(animationView)
        let scene = self.makeScene()
        animationView.presentScene(scene)
        animationView.backgroundColor = .clear
    }

    private func makeScene() -> SKScene {
        let size = CGSize(width: view.frame.width / 2, height: view.frame.height)
        let scene = SKScene(size: size)
        scene.backgroundColor = .clear

        self.addNodes(to: scene)
        return scene
    }

    private func addNodes(to scene: SKScene){
        let fisrtSptireNode = SKSpriteNode(color: .clear, size: CGSize(width: menuImageView.frame.width, height: menuImageView.frame.height))
        let submarineNode = SKSpriteNode(imageNamed: "submarineInMenu")
        fisrtSptireNode.position = CGPoint(x: -50, y: self.menuImageView.frame.origin.y)
        submarineNode.size = CGSize(width: menuImageView.frame.width, height: menuImageView.frame.height)
        scene.addChild(fisrtSptireNode)
        fisrtSptireNode.addChild(submarineNode)
        self.animateNodes(fisrtSptireNode)
    }

    private func animateNodes(_ nodes: SKSpriteNode) {
        let moveTo = SKAction.move(to: CGPoint(x: self.animationView.frame.width / 2, y: self.animationView.frame.height / 2), duration: 0.7)
            let moveGroup = SKAction.sequence([
                moveTo
            ])
            nodes.run(moveGroup)
    }
    
    @IBAction func startButtonPressed(_ sender: UIButton) {
        
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "GameViewController") as? GameViewController else {
            return
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func showAchievementsButtonPressed(_ sender: UIButton) {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "AchievementsViewController") as? AchievementsViewController else {
            return
        }
        self.navigationController?.pushViewController(controller, animated: true)

    }
    
    @IBAction func showSettingButtonPressed(_ sender: UIButton) {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController else{
            return
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    private func getSaves(){
        guard let result = results else {return}
        guard let settings = UserDefaults.standard.value(Settings.self, forKey: "GameSettings") else {return}
        
        Manager.shared.dateResultArray = result.dateResultArray
        Manager.shared.userNameArray = result.userNameArray
        Manager.shared.resultArray = result.resultArray
        Manager.shared.switchControl = settings.switchControl
    }
}

extension UIButton{
    func dropShadow() {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.2
        layer.shadowOffset = CGSize(width: 10, height: 10)
        layer.shadowRadius = 5
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
    }
}

extension UIView {
    func addParalaxEffect(amount: Int = 20) {
        let horizontal = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        horizontal.minimumRelativeValue = -amount
        horizontal.maximumRelativeValue = amount
        let vertical = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        vertical.minimumRelativeValue = -amount
        vertical.maximumRelativeValue = amount
        let group = UIMotionEffectGroup()
        group.motionEffects = [horizontal, vertical]
        addMotionEffect(group)
    }
}

